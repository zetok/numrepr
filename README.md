# numrepr [![build status](https://gitlab.com/zetok/numrepr/badges/master/build.svg)](https://gitlab.com/zetok/numrepr/commits/master)

**numrepr** shows representation of numbers in commonly used numeral systems.

Supported numeral systems:

* binary
* octal
* decimal
* hexadecimal

## Usage

```bash
$ numrepr $RANDOM
0b111111111 0o777 511 0x1ff

$ numrepr $RANDOM
0b10011000101110 0o23056 9774 0x262e

$ numrepr 0b1111_0000_1111 0o750 1_234_567_890 0xdeadbeef
0b111100001111 0o7417 3855 0xf0f
0b111101000 0o750 488 0x1e8
0b1001001100101100000001011010010 0o11145401322 1234567890 0x499602d2
0b11011110101011011011111011101111 0o33653337357 3735928559 0xdeadbeef
```

## Install

The easiest way is to use [`Cargo`]:

```bash
cargo install numrepr
```

If your distribution has been updated in recent years, it is very likely that
[`Cargo`] is already available. However if that's not the case, you can use
[Nix] package manager to get some newer stuff. Or just install [`Cargo`] without
using package manager. Whatever floats your boat.

## Updating

In case of newer version, updating is almost like installing:

```bash
cargo install --force numrepr
```

## License

Licensed under GPLv3+. For details, see [LICENSE].


[`Cargo`]: https://doc.rust-lang.org/cargo/
[LICENSE]: /LICENSE
[Nix]: https://nixos.org/nix/
