/*
    Copyright © 2018 Zetok Zalbavar <zexavexxe@gmail.com>

    This file is part of numrepr.

    numrepr is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    numrepr is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with numrepr.  If not, see <http://www.gnu.org/licenses/>.
 */



/*
TODO: try using stdin if there are no args supplied of if supplied arg is `-`
      since invoking executable takes time, and number of arguments supplied
      to it is limited, getting data from stdin is the only fastest way of
      working with large amounts of input data
TODO: option to ignore errors from input args – should still print errors to
stderr, but shouldn't end execution of the program
 */

// global, since it doesn't want to work properly with macros otherwise
#![allow(non_upper_case_globals)]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate failure;
#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate lazy_static;
extern crate regex;



use clap::Arg;
use failure::Error;
use regex::Regex;

use std::fmt::{Binary, Debug, Display, LowerHex, Octal};
use std::num::ParseIntError;


const INPUT_ARG: &str = "input_number";
const INPUT_SHORT_HELP: &str = "Number to show representations for.";
const INPUT_LONG_HELP: &str =
    "Supported input numeral systems are as follows: binary, octal,\n\
     decimal, hexadecimal; accepted format for them is respectively:\n\
     `0b1`, `0o1`, `1`, `0x1`.\n\
     \n\
     It is acceptable for longer numbers to have their digits\n\
     separated with `_` (underscore) for ease of reading, e.g.:\n\
     `0b_0010_1100_0101`, `123_456_789`.";


#[derive(Fail, Debug, Clone, Copy, Eq, PartialEq)]
#[fail(display = "Could not determine the numeral system")]
// TODO rename
struct NumeralDtrm;


#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Numeral {
    // associated number is radix
    Binary = 2,
    Octal = 8,
    Decimal = 10,
    Hexadecimal = 16
}

impl Numeral {
    fn determine(s: &str) -> Result<Self, NumeralDtrm> {
        macro_rules! detrm {
            ($($ename:ident, $rname:ident, $re:expr),+) => ($(
                lazy_static! {
                    static ref $rname: Regex = Regex::new($re).unwrap();
                }

                if $rname.is_match(s) {
                    return Ok(Numeral::$ename)
                }
            )+)
        }

        detrm!(Binary, RE_0b, "^0b[01]+$",
               Octal, RE_0o, "^0o[0-7]+$",
               Decimal, RE_dec, r"^\d+$",
               Hexadecimal, RE_0x, r"^0x[\da-f]+$");

        error!("Doesn't seem to be a supported number: '{}'", s);
        Err(NumeralDtrm)
    }

    /// Prefix of given Numeral variant.
    fn prefix(&self) -> &str {
        match self {
            Numeral::Binary => "0b",
            Numeral::Octal => "0o",
            Numeral::Decimal => "",
            Numeral::Hexadecimal => "0x",
        }
    }

    fn trim_prefix<'a>(&self, s: &'a str) -> &'a str {
        s.trim_left_matches(self.prefix())
    }

    fn radix(&self) -> u32 {
        *self as u32
    }

    fn parse_to_decimal(&self, s: &str) -> Result<u128, ParseIntError> {
        u128::from_str_radix(s, self.radix())
    }
}

/**
Prepare input to get it ready for parsing.

Preparing input removes preceding and trailing whitespaces and gets rid of all
`_`s (underscores) in the numbers.
*/
fn prepare_input(s: &str) -> String {
    let mut s = s.trim().replace("_", "");
    s.make_ascii_lowercase();
    s
}

// TODO write test
fn parse_input(i: &str) -> Result<u128, Error> {
    let i = &prepare_input(i);
    let n = Numeral::determine(i)?;
    Ok(n.parse_to_decimal(n.trim_prefix(i))?)
}


fn format_num<T>(n: T) -> String
where T: Binary + Debug + Display + LowerHex + Octal
{
    format!("{n:#b} {n:#o} {n} {n:#x}", n=n)
}


fn init_logger() {
    env_logger::Builder::from_default_env()
        .default_format_timestamp(false)
        .init();
}



fn main() {
    init_logger();

    let app = app_from_crate!()
        .arg(Arg::with_name(INPUT_ARG)
             .help(INPUT_SHORT_HELP)
             .long_help(&format!("{}\n\n{}", INPUT_SHORT_HELP, INPUT_LONG_HELP))
             .allow_hyphen_values(true)
             .required(true)
             .multiple(true)
        ).get_matches();


    for v in app.values_of(INPUT_ARG).unwrap() {
        match parse_input(v) {
            Ok(n) => println!("{}", format_num(n)),
            Err(e) => {
                error!("Parsing of '{}' failed: {}", v, e);
                std::process::exit(1);
            },
        }
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn format_num_test() {
        assert_eq!("0b0 0o0 0 0x0", &format_num(0));
        assert_eq!("0b1 0o1 1 0x1", &format_num(1));
        assert_eq!("0b1001 0o11 9 0x9", &format_num(9));
        assert_eq!("0b101000010 0o502 322 0x142", &format_num(322));
    }

    #[test]
    fn numeral_determine() {
        macro_rules! test_with {
            ($($n:expr)+) => ($(
                assert_eq!(Err(NumeralDtrm), Numeral::determine($n));
            )+);

            ($($out:ident, $n:expr),+) => ($(
                assert_eq!(Ok(Numeral::$out), Numeral::determine($n));
            )+);
        }

        test_with!(""
                   " "
                   "foo"
                   "0b"
                   "0o"
                   "0f");

        test_with!(Binary, "0b0",
                   Binary, "0b1",
                   Binary, "0b111111111",
                   Binary, "0b101110111");
        test_with!("0b111111211");

        (0..=7).for_each(|n| test_with!(Octal, &format!("{:#o}", n)));
        test_with!(Octal, "0o775",
                   Octal, "0o12345670");
        test_with!("0o8"
                   "0o9");

        test_with!(Decimal, "0",
                   Decimal, "1",
                   Decimal, "1234567890",

                   Hexadecimal, "0x1234567890abcdef",
                   Hexadecimal, "0x0",
                   Hexadecimal, "0x1",
                   Hexadecimal, "0xf0");
    }

    #[test]
    fn numeral_prefix() {
        macro_rules! np {
            ($($e:ident $out:expr),+) => ($(
                assert_eq!($out, Numeral::$e.prefix());
            )+)
        }

        np!(Binary "0b",
            Octal "0o",
            Decimal "",
            Hexadecimal "0x");
    }

    #[test]
    fn numeral_trim_prefix() {
        macro_rules! npfp {
            ($($in:expr, $out:expr, $enm:ident),+) => ($(
                assert_eq!($out, Numeral::$enm.trim_prefix($in));
            )+)
        }

        npfp!("0b", "", Binary,
              "0b", "0b", Octal,
              "0b", "0b", Decimal,
              "0b", "0b", Hexadecimal,
              "0b0", "0", Binary,
              "0b1", "1", Binary,
              "0b11111111", "11111111", Binary,
              "0b00000", "00000", Binary,

              "0o", "", Octal,
              "0o", "0o", Binary,
              "0o", "0o", Decimal,
              "0o", "0o", Hexadecimal,
              "0o0", "0", Octal,
              "0o1", "1", Octal,
              "0o2", "2", Octal,
              "0o3", "3", Octal,
              "0o4", "4", Octal,
              "0o5", "5", Octal,
              "0o6", "6", Octal,
              "0o7", "7", Octal,
              "0o2534252034567", "2534252034567", Octal,

              "0", "0", Decimal,
              "12345", "12345", Decimal,

              "0x", "", Hexadecimal,
              "0x", "0x", Binary,
              "0x", "0x", Octal,
              "0x", "0x", Decimal,
              "0x0", "0", Hexadecimal,
              "0x1", "1", Hexadecimal,
              "0x9", "9", Hexadecimal,
              "0xa", "a", Hexadecimal,
              "0xe", "e", Hexadecimal,
              "0xf", "f", Hexadecimal,
              "0x1af", "1af", Hexadecimal,
              "0xff", "ff", Hexadecimal,
              "0xdeadbeef", "deadbeef", Hexadecimal);
    }

    #[test]
    fn numeral_parse_to_decimal() {
        macro_rules! nptd {
            ($($e:ident, $in:expr, $out:expr),+) => ($(
                assert_eq!(Ok($out), Numeral::$e.parse_to_decimal($in));
            )+);

            ($($e:ident, $in:expr),+) => ($(
                assert!(Numeral::$e.parse_to_decimal($in).is_err());
            )+);

            ($e:ident ok $range:expr) => (
                $range.for_each(|n| nptd!($e, &format!("{}", n), n));
            );

            ($e:ident err $range:expr) => (
                $range.for_each(|n| nptd!($e, &format!("{}", n)));
            );
        }

        nptd!(Binary, "0", 0,
              Binary, "1", 1,
              Binary, "10", 2,
              Binary, "11", 3,
              Binary, "100", 4,
              Binary, "101", 5);
        nptd!(Binary err 2..=9);


        nptd!(Octal ok 0..=7);
        nptd!(Octal err 8..=9);

        nptd!(Decimal ok 0..123);

        nptd!(Hexadecimal ok 0..=9);
        // TODO complement test
    }


    #[test]
    fn _prepare_input() {
        assert_eq!("", prepare_input(" "));
        assert_eq!("", prepare_input("           "));
        assert_eq!("", prepare_input("\t\n\t_____\t\n\t"/*I'm dynamite*/));
        // invalid input, leaves space
        assert_eq!(" ", prepare_input("_ _"));
        assert_eq!("0b101", prepare_input(" \t\n\t0b1__0_1"));
        assert_eq!("0o123", prepare_input(" \t\n\t0o1__2_3"));
        assert_eq!("123", prepare_input(" \t\n\t1__2_3"));
        assert_eq!("0x123ffaa", prepare_input(" \t\n\t0x1__2_3ff_aa"));
    }

    #[test]
    fn _parse_input() {
        macro_rules! t {
            ($($in:expr, $err:ident),+) => ($(
                assert_eq!(parse_input($in).unwrap_err().downcast::<$err>().unwrap(),
                           $err);
            )+)
        }

        t!("", NumeralDtrm,
           " ", NumeralDtrm,
           "_", NumeralDtrm,
           "*", NumeralDtrm);
        // TODO complement test
    }

}
